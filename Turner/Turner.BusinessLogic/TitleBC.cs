﻿using System;
using System.Collections.Generic;
using System.Linq;
using Turner.Model;

namespace Turner.BusinessLogic
{
    public class TitleBC
    {
        #region private member
        public TitlesEntities DBContext { get; set; }
        #endregion

        #region constructor
        public TitleBC()
        {
            DBContext = new TitlesEntities();
        }

        public TitleBC(TitlesEntities titlesEntities)
        {
            DBContext = titlesEntities;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Get Title by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Title GetById(int id)
        {
            Title title = null;
            try
            {
                title = DBContext.Titles.Find(id);
            }
            catch (Exception)
            {
                //TODO implement logging
            }
            return title;
        }

        /// <summary>
        /// Search title by name
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        public List<Title> GetByName(string title)
        {
            List<Title> titleList = null;
            try
            {
                titleList = DBContext.Titles.Where(item => item.TitleName.Contains(title)).ToList();
            }
            catch (Exception)
            {
                //TODO implement logging
            }
            return titleList;
        }

        #endregion
    }
}