﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Turner.Web.UI.Controllers
{
    public class HomeController : Controller
    {
 
        /// <summary>
        /// Home Page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        /// <summary>
        /// Contact Page
        /// </summary>
        /// <returns></returns>
        public ActionResult Contact()
        {
            return View();
        }

    }
}
